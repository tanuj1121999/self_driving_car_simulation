#used for capturing top left part of screen
import numpy as np
from PIL import ImageGrab
import cv2
import time
from getKeys import key_check,listen_keys
import os
import threading

def key_to_output(keys):
    #[A,W,D]
    output=[0,0,0]

    if 'A' in keys:
        output=[1,0,0]
    elif 'D' in keys:
        output=[0,0,1]
    elif 'W' in keys:
        output=[0,1,0]

    return output

file_name='training_data.npy'

if os.path.isfile(file_name):
    training_data=list(np.load(file_name))
else:
    training_data=[]



def main():
    last_time=time.time()

    for i in list(range(4))[::-1]:
        print(i+1)
        time.sleep(1)


    t = threading.Thread(target=listen_keys)
    t.daemon = True
    t.start()

    while(True):
        screen =  np.array(ImageGrab.grab(bbox=(0,100,550,500)))
        screen = cv2.cvtColor(screen,cv2.COLOR_BGR2GRAY)
        screen=cv2.resize(screen,(80,60))
        keys=key_check()
        output=key_to_output(keys)
        training_data.append([screen,output])
        print(output)
        if len(training_data)%10 == 0:
            print(len(training_data))
            np.save(file_name,training_data)


        print(time.time()-last_time)
        last_time=time.time()

      

       


main()            