#used for capturing top left part of screen
import numpy as np
from PIL import ImageGrab
import cv2
import time
from getKeys import key_check,listen_keys
import os
from alexnet import alexnet
from direct_keys import PressKey,ReleaseKey,W,A,D,S

WIDTH=80
HEIGHT=60
LR=1e-3
EPOCH=8
MODEL_NAME='pysdc-{}-{}-{}-epochs.model'.format(LR,'alexnetv2',EPOCH)

def straight():
    PressKey(W)
    ReleaseKey(A)
    ReleaseKey(D)

def right():
    PressKey(D)
    time.sleep(0.8)
    ReleaseKey(W)
    ReleaseKey(A)
    ReleaseKey(D)

def left():
    PressKey(A)
    time.sleep(0.8) 
    ReleaseKey(W)
    ReleaseKey(D)
    ReleaseKey(A)


model=alexnet(WIDTH,HEIGHT,LR)
model.load(MODEL_NAME)

def main():
    last_time=time.time()

    for i in list(range(4))[::-1]:
        print(i+1)
        time.sleep(1)


    while(True):
        screen =  np.array(ImageGrab.grab(bbox=(0,100,550,500)))
        screen = cv2.cvtColor(screen,cv2.COLOR_BGR2GRAY)
        screen=cv2.resize(screen,(80,60))

        prediction=model.predict([screen.reshape(WIDTH,HEIGHT,1)])[0]
        moves=list(np.around(prediction))
        print(moves,prediction)

        if moves==[1,0,0]:
            left()
        elif moves==[0,0,1]:
             right()
        elif moves ==[0,1,0]:
             straight()
       



        print(time.time()-last_time)
        last_time=time.time()

      

       


main()            